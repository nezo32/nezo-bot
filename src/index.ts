import { CallbackService, VK } from 'vk-io';
import { DirectAuthorization, officialAppCredentials } from '@vk-io/authorization';
import { Telegraf } from 'telegraf';

const callbackService = new CallbackService();

const direct = new DirectAuthorization({
    callbackService,

    scope: 'all',
    ...officialAppCredentials.android,

    login: process.env.LOGIN,
    password: process.env.PASSWORD,

    apiVersion: '5.131'
});

const client_info = await direct.run();

const vk = new VK({ token: client_info.token })
const tg = new Telegraf(process.env.TG_TOKEN)

const tg_receiver_id = process.env.RECEIVER_ID;

vk.updates.on('message_new', async (ctx) => {
    let send_message = false;
    let sender = "";


    if (ctx.isChat) {
        const conversation = (await vk.api.messages.getConversationsById({ peer_ids: ctx.peerId })).items[0]
        if (conversation?.push_settings?.disabled_forever) return;
        sender = conversation?.chat_settings?.title;
        send_message = true;
    }
    else if (ctx.isDM) {
        const chat_info = (await vk.api.users.get({ user_ids: [ctx?.senderId] }))[0];
        sender = chat_info?.first_name + " " + chat_info?.last_name;
        send_message = true;
    }

    if (send_message) await tg.telegram.sendMessage(tg_receiver_id, `${sender} chat:\n\n${ctx.text}`, {

    });
})

await vk.updates.start();

process.once('SIGINT', async () => {
    await vk.updates.stop();

});
process.once('SIGTERM', async () => {
    await vk.updates.stop();
});
